# Ball play NOW
---
This is an Android app that introduced random team-matching on the idea of Location-Based Service (LBS).

People who are located within 5 km of the playing spot will receive in-app notifications for upcoming events.

App demo: https://drive.google.com/file/d/1XofdnXpxhSawSh_5awP_WZIrFizgW7wt/view?usp=sharing