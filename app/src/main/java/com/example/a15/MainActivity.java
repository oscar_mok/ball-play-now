package com.example.a15;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Button mBtn_fd_list;
    private Button mBtn_manage_invitation;
    private Button mBtn_start_invitation;
    private Button mBtn_feedback;
    private FloatingActionButton mBtn_edit_information;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference FIREbaseDatebase;
    private Button deleteAccount;
    private ImageView ProfileImage;
    private static final int PERMISSIONS_REQUEST = 100;


    static MainActivity mainActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = this;

        //Logout function + Check current user
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        //User must be logged in before having the permission to access front page of the APP
        if (firebaseUser == null) {

            Intent intent = new Intent(getApplicationContext(), login.class);
            startActivity(intent);

        } else {


            setContentView(R.layout.activity_main);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            FloatingActionButton fab = findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            NavigationView navigationView = findViewById(R.id.nav_view);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            navigationView.setNavigationItemSelectedListener(this);

            View headerView = navigationView.getHeaderView(0);

            //Database Reference
            FIREbaseDatebase = FirebaseDatabase.getInstance().getReference();

            //Create new child in lastOnline with key is UID （for location tracking)
            FirebaseDatabase.getInstance().getReference("lastOnline").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("email").setValue(FirebaseAuth.getInstance().getCurrentUser().getEmail());

            //Display user's information on the front page
            final TextView mTextView_name = (TextView) findViewById(R.id.textView_name);
            final TextView mTextView_phone = (TextView) findViewById(R.id.textView_phone);
            final TextView mTextView_email = (TextView) findViewById(R.id.textView_email);
            final TextView mTextView_sex = (TextView) findViewById(R.id.textView_sex);
            final TextView mTextView_birthday = (TextView) findViewById(R.id.textView_birthday);
            final TextView mTextView_basketball = (TextView) findViewById(R.id.textView_basketball);
            final TextView mTextView_volleyball = (TextView) findViewById(R.id.textView_volleyball);
            final TextView mTextView_football = (TextView) findViewById(R.id.textView_football);
            final ImageView mImageView = (ImageView) findViewById(R.id.imageView4);
            final TextView NavProfileUserName = (TextView)headerView.findViewById(R.id.nav_user_full_name);
            final CircleImageView NavProfileImage = (CircleImageView)headerView.findViewById(R.id.nav_profile_image);

            FIREbaseDatebase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String string = dataSnapshot.child("User").child(firebaseUser.getUid()).child("Name").getValue(String.class);
                        mTextView_name.setText(string);
                        NavProfileUserName.setText(string);

                        string = dataSnapshot.child("User").child(firebaseUser.getUid()).child("Phone").getValue(String.class);
                        mTextView_phone.setText(string);

                        string = dataSnapshot.child("User").child(firebaseUser.getUid()).child("Email").getValue(String.class);
                        mTextView_email.setText(string);

                        string = dataSnapshot.child("User").child(firebaseUser.getUid()).child("Sex").getValue(String.class);
                        mTextView_sex.setText(string);

                        string = dataSnapshot.child("User").child(firebaseUser.getUid()).child("Birthday").getValue(String.class);
                        mTextView_birthday.setText(string);

                        string = dataSnapshot.child("User").child(firebaseUser.getUid()).child("Basketball_LV").getValue(String.class);
                        mTextView_basketball.setText(string);

                        string = dataSnapshot.child("User").child(firebaseUser.getUid()).child("Volleyball_LV").getValue(String.class);
                        mTextView_volleyball.setText(string);

                        string = dataSnapshot.child("User").child(firebaseUser.getUid()).child("Football_LV").getValue(String.class);
                        mTextView_football.setText(string);

                        //ProfileImage
                        if (dataSnapshot.child("User").child(firebaseUser.getUid()).child("ProfileImage").exists()) {
                            string = dataSnapshot.child("User").child(firebaseUser.getUid()).child("ProfileImage").getValue(String.class);
                            Picasso.get().load(string).into(mImageView);
                            Picasso.get().load(string).into(NavProfileImage);
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            //End of "Display user's information on the front page"

            //建立換頁功能
            mBtn_fd_list = (Button) findViewById(R.id.btn_fd_list);
            mBtn_manage_invitation = (Button) findViewById(R.id.btn_manage_invitation);
            mBtn_start_invitation = (Button) findViewById(R.id.btn_start_invitation);
            mBtn_feedback = (Button) findViewById(R.id.btn_feedback);
            mBtn_edit_information = (FloatingActionButton) findViewById(R.id.floatingActionButton);


            mBtn_fd_list.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), FindFriendActivity.class);
                    startActivity(intent);
                }
            });

            mBtn_manage_invitation.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), check_member.class);
                    startActivity(intent);
                }
            });

            mBtn_start_invitation.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), invitation_step1.class);
                    startActivity(intent);
                }
            });

            mBtn_feedback.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent intent = new Intent(getApplicationContext(), review.class);
                    Intent intent = new Intent(getApplicationContext(), review_hardcoded.class);
                    startActivity(intent);
                }
            });

            mBtn_edit_information.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), edit_information.class);
                    startActivity(intent);
                }
            });
            //換頁功能完結

            //Test btn
            Button testbtn = (Button) findViewById(R.id.testbtn);

            testbtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ListOnline2.class);
                    startActivity(intent);
                }
            });
            //Test btn

//          !!!!!!!!!!!!!    Track location !!!!!!!!!!!!!!!!!!!!!
            //Check whether GPS tracking is enabled//

            LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                finish();
            }

//Check whether this app has access to the location permission//

            int permission = ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION);

//If the location permission has been granted, then start the TrackerService//

            if (permission == PackageManager.PERMISSION_GRANTED) {
                startTrackerService();
            } else {

//If the app doesn't currently have access to the user’s location, then request access//

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST);
            }
        }

    }

//Start the TrackerService//

    private void startTrackerService () {
        startService(new Intent(this, TrackingService.class));

//Notify the user that tracking has been enabled//

        //Toast.makeText(this, "GPS tracking enabled", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult ( int requestCode, String[] permissions,int[]
            grantResults){

//If the permission has been granted...//

        if (requestCode == PERMISSIONS_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

//...then start the GPS tracking service//

            startTrackerService();
        } else {

//If the user denies the permission request, then display a toast with some more information//

            Toast.makeText(this, "請允許定位功能，否則將無法接收邀約...", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.nav_logout) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_check_invitation) {

            Intent intent = new Intent(getApplicationContext(), check_member.class);
            startActivity(intent);

        } else if (id == R.id.nav_edit_member_information) {


            Intent intent = new Intent(getApplicationContext(), edit_information.class);
            startActivity(intent);

        } else if (id == R.id.nav_send_review) {

            Intent intent = new Intent(getApplicationContext(), give_review.class);
            startActivity(intent);

        }else if (id == R.id.nav_friend){

            Intent intent = new Intent(getApplicationContext(), friends.class);
            startActivity(intent);

        } else if (id == R.id.nav_logout) {

            FirebaseAuth.getInstance().signOut();
            Toast.makeText(getApplicationContext(), "登出成功", Toast.LENGTH_SHORT).show();
            //TrackingService.getTrackingService().stopSelf();
            Intent intent = new Intent(MainActivity.this, login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        } else if (id == R.id.nav_del_acc) {

            AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
            dialog.setTitle("Are you sure?");
            dialog.setMessage("Deleting this account will result in completely removing your account from the system and you won't be able to access the app.");
            dialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    firebaseUser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful())
                            {
                                Toast.makeText(MainActivity.this,"Account Deleted", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(MainActivity.this,login.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                            else {
                                Toast.makeText(MainActivity.this,task.getException().getMessage(),Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            });
            dialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            AlertDialog alertDialog = dialog.create();
            alertDialog.show();

        }else if (id == R.id.nav_share) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private  void  SendUserToFindfriendActivity(){
        Intent loginIntent = new Intent(MainActivity.this, FindFriendActivity.class);
        startActivity(loginIntent);
    }


    public static MainActivity getInstance(){
        return   mainActivity;
    }
}

