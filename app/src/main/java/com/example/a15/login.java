package com.example.a15;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class login extends AppCompatActivity {
    private Button mButton_register_;
    private Button mButton_login;
    private EditText mEditText_email;
    private TextInputLayout mEditText_password;
    private TextView forgetpassword_link;

    private Boolean emailAddressChecker;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String email;
    private String password;
    private GoogleSignInClient mGoogleSignInClient;
    private SignInButton mSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        findviewById();

        //忘記密碼
        forgetpassword_link.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(login.this, ResetPasswordActivity.class));
            }
        });

        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, 101);
            }
        });

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this,gso);

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("TAG",user.getUid());
                    startActivity(new Intent(login.this,MainActivity.class));
                } else {
                    // User is signed out
                    Log.d("Tag","user ==null");
                }
            }
        };
    }
    private View.OnClickListener btnListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {

            if (view.getId()==R.id.btnlogin){
                String email = mEditText_email.getText().toString();
                String password = mEditText_password.getEditText().getText().toString().trim();

                //email帳號為空，跳出警告
                if(email.isEmpty()){
                    mEditText_email.setError("Please Enter Email");
                    mEditText_email.requestFocus();
                }

                //password為空，跳出警告
                else if(password.isEmpty()) {
                    mEditText_password.setError("Please Enter Password");
                    mEditText_password.requestFocus();
                }


                //若都不為空值，開始檢驗帳密是否符合
                else if(!(email.isEmpty() && password.isEmpty())) {

                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        //Toast message indicate that login is successful
                                        Toast.makeText(getApplicationContext(), "登入成功", Toast.LENGTH_LONG).show();

                                        MainActivity.getInstance().finish();
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(intent);
                                    } else
                                        Toast.makeText(getApplicationContext(), "帳號或密碼錯誤", Toast.LENGTH_LONG).show();
                                }
                            });
                }
            }
            if (view.getId()==R.id.btnregister){
                startActivity(new Intent(login.this,new_member_information.class));
            }
        }
    };
    private void findviewById() {
        forgetpassword_link = (TextView)findViewById(R.id.forget_password_link) ;
        mButton_register_ = (Button)findViewById(R.id.btnregister);
        mButton_login = (Button)findViewById(R.id.btnlogin);
        mEditText_email = (EditText)findViewById(R.id.edtemail);
        mEditText_password = (TextInputLayout) findViewById(R.id.edtpassword);
        mSignInButton = (SignInButton) findViewById(R.id.mygooglebutton);
        mButton_register_.setOnClickListener(btnListener);
        mButton_login.setOnClickListener(btnListener);

    }

    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == 101) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately

                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();

                            Intent intent = new Intent(login.this, MainActivity.class);
                            startActivity(intent);
                            Toast.makeText(login.this, "登入成功", Toast.LENGTH_LONG).show();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(login.this, "登入失敗", Toast.LENGTH_LONG).show();
                        }

                        // ...
                    }
                });
    }
}
