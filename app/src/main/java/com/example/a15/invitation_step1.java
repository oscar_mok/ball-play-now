package com.example.a15;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class invitation_step1 extends AppCompatActivity {
    private Button mbtn_start_invitation_fd;
    private Spinner mSpinner_select_sport;
    private static String selected_sport = null;
    private FirebaseDatabase FIREbaseDatebase;
    private DatabaseReference FIREBASEDatebase;
    private Button mbtn_start_invitation_stranger;
    private Spinner mSpinner_select_num_of_ppl;
    private static String selected_num_of_ppl = null;
    //記錄組隊方式
    private static int activity_choose = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.invitation_step1);

        FIREbaseDatebase = FirebaseDatabase.getInstance();

        mbtn_start_invitation_fd = (Button) findViewById(R.id.btn_friend);
        mbtn_start_invitation_stranger = (Button) findViewById(R.id.btn_stranger);
        mSpinner_select_sport = (Spinner) findViewById(R.id.spinner_select_sport);
        mSpinner_select_num_of_ppl = (Spinner) findViewById(R.id.spinner_num_of_ppl);

        mSpinner_select_num_of_ppl.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //set text size & color
                ((TextView) parent.getChildAt(0)).setTextSize(30);
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);

                //讀取spinner 的選取值
                selected_num_of_ppl = parent.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSpinner_select_sport.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //set text size & color
                ((TextView) parent.getChildAt(0)).setTextSize(30);
                ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);

                //讀取spinner 的選取值
                selected_sport = parent.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mbtn_start_invitation_fd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Get CurrentUser from Auth. server
                FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

                //Setting path: User -> UID
                //currentFirebaseUser.getUid() <--- Can get UID of user's email
                FIREBASEDatebase = FIREbaseDatebase.getReference("User").child(currentFirebaseUser.getUid());

                //Write data into database
//                FIREBASEDatebase.child("Invitation").child("Sport").setValue(selected_sport);

                //!!!!!!  spinner 不能沒有選取任何運動 !!!!!!!!!
                if (selected_num_of_ppl.matches("") && selected_sport.matches("")){
                    Toast.makeText(getApplicationContext(), "請選擇運動及邀約人數", Toast.LENGTH_SHORT).show();
                }
                else if (selected_sport.matches("")) {
                    Toast.makeText(getApplicationContext(), "請選擇運動", Toast.LENGTH_SHORT).show();
                }else if (selected_num_of_ppl.matches("")){
                    Toast.makeText(getApplicationContext(), "請選擇邀約人數", Toast.LENGTH_SHORT).show();
                }else {
                    activity_choose = 0;
                    Intent intent = new Intent(getApplicationContext(), invitation_step2.class);
                    startActivity(intent);
                }
            }
        });

        mbtn_start_invitation_stranger.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //隨機找隊友  (未完成

                //!!!!!!  spinner 不能沒有選取任何運動 !!!!!!!!!
                if (selected_num_of_ppl.matches("") && selected_sport.matches("")){
                    Toast.makeText(getApplicationContext(), "請選擇運動及邀約人數", Toast.LENGTH_SHORT).show();
                }
                else if (selected_sport.matches("")) {
                    Toast.makeText(getApplicationContext(), "請選擇運動", Toast.LENGTH_SHORT).show();
                }else if (selected_num_of_ppl.matches("")){
                    Toast.makeText(getApplicationContext(), "請選擇邀約人數", Toast.LENGTH_SHORT).show();
                }else {
                    activity_choose = 1;
                    Intent intent = new Intent(getApplicationContext(), Random_SD_Invitation.class);
                    startActivity(intent);
                }
            }
        });


    }

    public static int getActivity_choose() {
        return activity_choose;
    }

    public static String get_SelectedSport(){
        return selected_sport;
    }

    public static String getSelected_num_of_ppl() {
        return selected_num_of_ppl;
    }
}
