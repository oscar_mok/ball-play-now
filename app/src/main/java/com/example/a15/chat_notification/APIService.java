package com.example.a15.chat_notification;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers({
            "Content_Type:application/json",
            "Authorization:key=AAAA5Uvt-kg:APA91bHOBfdlELC_bPU-p8vrbTzU3nr_beRFXMrtWTW85dGtBX0O2ypHgO_sekKfTx5yrwgYHwV2a0A9t3npqAGtR8h0Sn7Z5I6vS0Jd-JBfFV5yxrPHdkAFxAEKZUfRNVNN_2c7owPe"
    })

    @POST("fcm/send")
    Call<Response> sendNotification(@Body Sender body);
}
