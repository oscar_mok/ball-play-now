package com.example.a15.chat_notification;

public class ModelUser {
    //Use same name as in firebase database
    String Name, Email,ProfileImage;

    public ModelUser(){
    }

    public ModelUser(String name, String email, String profileImage) {
        Name = name;
        Email = email;
        ProfileImage = profileImage;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }
}
