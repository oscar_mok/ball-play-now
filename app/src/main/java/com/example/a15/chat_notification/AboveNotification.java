package com.example.a15.chat_notification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.net.Uri;
import android.os.Build;

import java.util.jar.Attributes;

public class AboveNotification extends ContextWrapper {
    private static final String ID = "some_id";
    private static final String NAME = "Balling";

    private NotificationManager notificationManager;

    public AboveNotification(Context base) {
        super(base);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            createChannel();
            ChannelInvitation();
        }
    }

    //建立通知頻道
    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel() {
        NotificationChannel notificationChannel = new NotificationChannel(ID ,NAME, NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.enableLights(true);
        notificationChannel.enableVibration(true);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        //加入頻道
        getManager().createNotificationChannel(notificationChannel);
    }

    //建立邀約通知頻道
    @TargetApi(Build.VERSION_CODES.O)
    private void ChannelInvitation() {
        NotificationChannel notificationChannel_invite = new NotificationChannel(ID ,NAME, NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel_invite.enableLights(true);
        notificationChannel_invite.enableVibration(true);
        notificationChannel_invite.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        //加入頻道
        getManager().createNotificationChannel(notificationChannel_invite);
    }

    private NotificationManager getManager() {
        if (notificationManager == null){
            notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public Notification.Builder getONotifications(String title,
                                                 String body,
                                                 Uri soundUri,
                                                 String icon){
        return new Notification.Builder(getApplicationContext(), ID)
                .setContentTitle(title)  //設定通知內容的標題
                .setContentText(body)   //設定通知內容的訊息
                .setSound(soundUri)
                .setAutoCancel(true)
                .setSmallIcon(Integer.parseInt(icon));
    }

    //邀約功能的通知
    @TargetApi(Build.VERSION_CODES.O)
    public Notification.Builder getNotifications(String title_main,
                                                 String body_main,
                                                 Uri soundUri_main,
                                                 String icon_main){
        return new Notification.Builder(getApplicationContext(), ID)
                .setContentTitle(title_main)
                .setContentText(body_main)
                .setSound(soundUri_main)
                .setAutoCancel(true)
                .setSmallIcon(Integer.parseInt(icon_main));
    }
}
