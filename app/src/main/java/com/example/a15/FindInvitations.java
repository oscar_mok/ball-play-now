package com.example.a15;

public class FindInvitations {
    private String Date;
    private String Sport;
    private String Start_Time;
    private String Place_name;
    private String Age_Restriction;
    private String Sport_Level_Maximum;
    private String Sport_Level_Minimum;

    public FindInvitations() {
    }  //Needs for Firebase

    public FindInvitations(String date, String sport, String start_Time, String place_name, String age_Restriction, String sport_Level_Maximum, String sport_Level_Minimum) {
        Date = date;
        Sport = sport;
        Start_Time = start_Time;
        Place_name = place_name;
        Age_Restriction = age_Restriction;
        Sport_Level_Maximum = sport_Level_Maximum;
        Sport_Level_Minimum = sport_Level_Minimum;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getSport() {
        return Sport;
    }

    public void setSport(String sport) {
        Sport = sport;
    }

    public String getStart_Time() {
        return Start_Time;
    }

    public void setStart_Time(String start_Time) {
        Start_Time = start_Time;
    }

    public String getPlace_name() {
        return Place_name;
    }

    public void setPlace_name(String place_name) {
        Place_name = place_name;
    }

    public String getAge_Restriction() {
        return Age_Restriction;
    }

    public void setAge_Restriction(String age_Restriction) {
        Age_Restriction = age_Restriction;
    }

    public String getSport_Level_Maximum() {
        return Sport_Level_Maximum;
    }

    public void setSport_Level_Maximum(String sport_Level_Maximum) {
        Sport_Level_Maximum = sport_Level_Maximum;
    }

    public String getSport_Level_Minimum() {
        return Sport_Level_Minimum;
    }

    public void setSport_Level_Minimum(String sport_Level_Minimum) {
        Sport_Level_Minimum = sport_Level_Minimum;
    }
}
