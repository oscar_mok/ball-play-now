package com.example.a15;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.Place.Field;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.security.KeyStore;
import java.util.Arrays;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap; //宣告google map物件
    float zoom;
    private EditText mSearchText;

    private LocationManager locMgr;
    String bestProv;
    private LatLng latLng;
    private Marker marker;
    private FloatingActionButton mSubmit_btn;

    public static double getLatitude_final() {
        return latitude_final;
    }

    public static double getLongtude_final() {
        return longtude_final;
    }

    public static String getPlace_name() {
        return place_name;
    }

    public static String getPlace_id() {
        return place_id;
    }

    public static String getPlace_address() {
        return place_address;
    }

    private static double latitude_final, longtude_final;
    private static String place_name, place_id, place_address;

    @Override
    protected void onResume() {
        super.onResume();
        locMgr = (LocationManager)getSystemService(Context.LOCATION_SERVICE);  //取得定位服務
        Criteria criteria = new Criteria();                              //利用criteria物件取得最佳定位方式
        bestProv = locMgr.getBestProvider(criteria, true);  //取得最佳定位方式

        //如果GPS或網路訂位開啟，更新位置
        if (locMgr.isProviderEnabled(LocationManager.GPS_PROVIDER)||
        locMgr.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            //確認ACCESS_FINE_LOCATION 權限是否授權
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
                locMgr.requestLocationUpdates(bestProv, 1000, 1, this);
            }
        }
        else{
            Toast.makeText(this, "請開啟定位服務", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        //確認ACCESS_FINE_LOCATION 權限是否授權
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locMgr.removeUpdates(this);
        }
    }

    //Newly added
    PlacesClient placesClient;
    //Newly added (End)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mSubmit_btn = (FloatingActionButton) findViewById(R.id.floatingActionButton_step3);

        mSubmit_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (latLng != null){
                    // Prompt after user pressed submit button
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case DialogInterface.BUTTON_POSITIVE:
                                    //Yes button clicked
                                    Intent intent = new Intent(getApplicationContext(), invitation_step4.class);
                                    startActivity(intent);
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
                    builder.setMessage("您確定選擇這個地點嗎？")
                            .setPositiveButton("是", dialogClickListener)
                            .setNegativeButton("否", dialogClickListener).show();
                }else {
                    Toast.makeText(getApplicationContext(), "請先選取地點", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //mSearchText =(EditText)findViewById(R.id.input_search);

        //Newly added
        String apikey = "AIzaSyA3Fmybtg9af7pwQFLOv8o8xxs3qIFx7Qk";
//                AIzaSyB3l5Ez04aA-GkfNwh1nN0UGb0Ct3gPIX4
        if (!Places.isInitialized()){
            Places.initialize(getApplicationContext(), apikey);
        }

        placesClient = Places.createClient(this);

        final AutocompleteSupportFragment autocompleteSupportFragment =
                (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.map_auto_complete);

        autocompleteSupportFragment.getView().setBackgroundColor(Color.WHITE);

        //SetPlaceFields -> 記錄選擇的地點
        autocompleteSupportFragment.setPlaceFields((Arrays.asList(Place.Field.ID, Field.LAT_LNG, Place.Field.NAME)));

        autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                latLng = place.getLatLng();

                Log.i("PlaceApi:   ", "onPlaceSelected: " + latLng.latitude + "\n" + latLng.longitude);
                // TODO: Get info about the selected place.
                Log.i("PlaceApi:   ", "Place: " + place.getName() + ", " + place.getId());

                // 寫入 使用者選擇的地址
                latitude_final = latLng.latitude;
                longtude_final = latLng.longitude;
                place_name = place.getName();
                place_id = place.getId();
                place_address = place.getAddress();

//                Log.i("PlaceApi:   ", "Placeselected:   " + latitude_final + "\n" + longtude_final + "\n" + place_name +"\n" + place_address);

                if (latLng != null) {
                    // Add a marker in NCU and move the camera
                    LatLng selected_place = new LatLng(latLng.latitude, latLng.longitude);
                    if (marker == null){
                    marker = mMap.addMarker(new MarkerOptions().position(selected_place).title("您選擇的位置"));
                    marker.setDraggable(false); //標記不可移動
                    marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)); //更改標記顏色
                    }else {
                        marker.remove();
                        marker = mMap.addMarker(new MarkerOptions().position(selected_place).title("您選擇的位置"));
                    }
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(selected_place, zoom));
                }
            }

            @Override
            public void onError(@NonNull Status status) {

            }
        });
        //Newly added (End)

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        //取得GoogleMaps物件
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL); //一般地圖

        LatLng Point = new LatLng(23.5982979, 120.8353631);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Point,6));


        //檢查授權
        requestPermission();

        mMap.getUiSettings().setZoomControlsEnabled(true); //顯示縮放圖示

        mMap.getUiSettings().setRotateGesturesEnabled(true); //可用手勢控制地圖旋轉
        mMap.getUiSettings().setScrollGesturesEnabled(true); //可用手勢控制地圖左右移動
        mMap.getUiSettings().setZoomGesturesEnabled(true); //可使用手勢控制地圖縮放
//        mMap.getUiSettings().setMyLocationButtonEnabled(true);//顯示定位按鈕

        //設定中心點&放大倍率
        //float zoom=17;
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(NCU, zoom));
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >=23){  //Androids 6.0以上
            // 判斷是否已取得授權
            int hasPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

            if (hasPermission != PackageManager.PERMISSION_GRANTED){ //未取得授權
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
                return;
            }
        }

        //如果裝置版本是Androids 6.0以下
        //如果裝置版本是6.0(包含)以上，使用者已授權

        setMyLocation(); //顯示定位圖層
    }

    private void setMyLocation()throws SecurityException {
        mMap.setMyLocationEnabled(true); //顯示定位圖層
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        //確認授權碼為1才處理
        if (requestCode == 1){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){ //按允許鈕
                setMyLocation(); //顯示定位圖層
            }
            else{ //按拒絕鈕
                Toast.makeText(this,"未取得授權", Toast.LENGTH_SHORT).show();
                finish(); //結束應用程式
            }
        }
        else{
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //取得地圖座標值：緯度, 經度
        //String x = "緯= " +Double.toString(location.getLatitude());
        //String y = "經= " +Double.toString(location.getLongitude());
        zoom = 16;
        mMap.setMyLocationEnabled(true);//顯示定位圖示

        //Toast.makeText(this, x +"\n"+ y, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Criteria criteria = new Criteria();
        bestProv = locMgr.getBestProvider(criteria, true);
    }

    @Override
    public void onProviderEnabled(String provider) {
//        Toast.makeText(getApplicationContext(), "定位功能已開啟", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
//        Toast.makeText(getApplicationContext(), "定位功能未開啟", Toast.LENGTH_SHORT).show();
    }
}
