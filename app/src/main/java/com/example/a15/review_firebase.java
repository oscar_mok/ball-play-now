package com.example.a15;

public class review_firebase {
    private String ProfileImage;
    private String Name;

    public review_firebase() {
    }  //Needs for Firebase


    public review_firebase(String profileImage, String name) {
        ProfileImage = profileImage;
        Name = name;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
