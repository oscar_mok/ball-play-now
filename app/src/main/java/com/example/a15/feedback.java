/*
package com.example.a15;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

public class feedback extends AppCompatActivity {

    private RatingBar mRating;
    private TextView mTextView_UserName;
    private float user_rating;
    private TextView mTextView_stars;
    private DatabaseReference FIREbaseDatebase;
    private FirebaseUser currentFirebaseUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.feedback);

        mTextView_stars = (TextView) findViewById(R.id.textView_stars);
        mRating = (RatingBar) findViewById(R.id.ratingBar2);
        mTextView_UserName = (TextView) findViewById(R.id.textView_UserName);

        FIREbaseDatebase = FirebaseDatabase.getInstance().getReference();
        currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;

        //!!!!!!!!!   mTextView_UserName 更改成用戶自己的User name   !!!!!!!!
        FIREbaseDatebase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String string = dataSnapshot.child("User").child(currentFirebaseUser.getUid()).child("Name").getValue(String.class);
                    mTextView_UserName.setText(string);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        //文字顯示user 有幾顆星評分
        // ***replace "4.3" with actual rating value in database ***
        //mTextView_stars.setText("4.3 ");


        //!!!!!!!!  把用戶的平均Rating assign 到 user_rating變數中 (待寫）  !!!!!!!!
        // ***replace "4.3" with actual rating value in database ***
        //user_rating = (float) 4.3;

        //RatingBar(評分) 的value 存放在"rating"
        //把rating從database放到RatingBar 顯示
        mRating.setRating(user_rating);
    }
}

 */
