package com.example.a15;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


public class friends extends AppCompatActivity {

    private RecyclerView myFriendList;
    private DatabaseReference FriendsRef, UsersRef;
    private FirebaseAuth mAuth;
    private String online_user_id;
    private FirebaseRecyclerAdapter<FindFriends, FriendsViewHolder> findFriendsFriendsViewHolderFirebaseRecyclerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friend_list);

        mAuth = FirebaseAuth.getInstance();
        online_user_id = mAuth.getCurrentUser().getUid();
        FriendsRef = FirebaseDatabase.getInstance().getReference().child("Friends").child(online_user_id);
        UsersRef = FirebaseDatabase.getInstance().getReference().child("User");

        myFriendList = (RecyclerView)findViewById(R.id.friend_list);
        //myFriendList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        myFriendList.setLayoutManager(linearLayoutManager);

        DisplayAllFriends();

    }

    private void DisplayAllFriends() {

        FirebaseRecyclerOptions<FindFriends> options = new FirebaseRecyclerOptions.Builder<FindFriends>().setQuery(FriendsRef, FindFriends.class).build();

        findFriendsFriendsViewHolderFirebaseRecyclerAdapter
                = new FirebaseRecyclerAdapter<FindFriends, FriendsViewHolder>(options) {

            @NonNull
            @Override
            public FriendsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                friends.super.onStart();

                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_users_display_layout, parent, false);

                return new FriendsViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull final FriendsViewHolder friendsViewHolder, int i, @NonNull FindFriends findFriends) {
                final String userIDs = getRef(i).getKey();

                FriendsRef.child(userIDs).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){


                            Log.d("Print: ", userIDs);

                            friendsViewHolder.date.setText("Friends since: " + dataSnapshot.child("date").getValue().toString());

                            friendsViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    /*Click user from friend list to start chatting/messaging
                                    Start activity by putting UID of receiver
                                    Use the UID to identify the user we are gonna chat*/

                                    //Return to current RecyclerView when user pressed "back"
                                    finish();
                                    Intent intent2 = new Intent(friends.this, friends.class);
                                    intent2.putExtra("node", userIDs);
                                    startActivity(intent2);

                                    Intent intent = new Intent(friends.this,ChatActivity.class);
                                    intent.putExtra("visit_user_id",userIDs);
                                    startActivity(intent);
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                UsersRef.child(userIDs).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            final String userName = dataSnapshot.child("Name").getValue().toString();

                            friendsViewHolder.name.setText(userName);


                            //Prevent error if user have no profile image
                            if (dataSnapshot.child("ProfileImage").exists()) {
                                final String profileImage = dataSnapshot.child("ProfileImage").getValue().toString();
                                friendsViewHolder.setProfileImage(getApplicationContext(), profileImage);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        };
        findFriendsFriendsViewHolderFirebaseRecyclerAdapter.startListening();
        myFriendList.setAdapter(findFriendsFriendsViewHolderFirebaseRecyclerAdapter);

        }

    private class FriendsViewHolder extends RecyclerView.ViewHolder{
        TextView name, date;

        public void setProfileImage(Context ctx, String profileImage){
            ImageView mimage = (ImageView)itemView.findViewById(R.id.imageIv);
            Picasso.get().load(profileImage).into(mimage);
        }

        public FriendsViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name_Tv);
            date = itemView.findViewById(R.id.statusTv);
        }
    }

    @Override
    protected void onStop() {
        if(findFriendsFriendsViewHolderFirebaseRecyclerAdapter != null)
            findFriendsFriendsViewHolderFirebaseRecyclerAdapter.stopListening();
        super.onStop();
    }
}

