package com.example.a15;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Random_SD_Invitation extends AppCompatActivity {
    private DatabaseReference location_n_UID;
    private DatabaseReference locations;
    private String curr_lat;
    private String curr_lng;
    private static double curr_lat_int;
    private static double curr_lng_int;
    private String lat;
    private String lng;
    private double lat_int;
    private double lng_int;
    private List<Double> distance = new ArrayList<Double>();
    private static List<String> all_UID = new ArrayList<String>();
    private int k = 0;
    private String currentUser_UID;
    private static List<String> UID_selected;
    private static Random_SD_Invitation mContent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContent = this;

        currentUser_UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        locations = FirebaseDatabase.getInstance().getReference("Locations");
        location_n_UID = FirebaseDatabase.getInstance().getReference("Location_" + currentUser_UID);

        //Clear list values (if any)
        all_UID.clear();
        distance.clear();

        alluserlocation();

        //Write into database
        locations.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    location_n_UID.child(snapshot.getKey()).child("Distance").setValue(distance.get(k));
                    k++;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //Sort array
//        Arrays.sort(new List[]{distance});
        Collections.sort(distance);

//        for (int i = 0; i < distance.size(); i++){
//            Log.d("TAG", String.valueOf(distance.get(i)));
//        }


        //與database 記錄作比較，找出相對的UID

        // Distance with others users
        location_n_UID.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Get Lat & Lng values (selected user)
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (distance.contains(Double.parseDouble(snapshot.child("Distance").getValue().toString()))){
                        all_UID.add(snapshot.getKey());
                        Log.d("TAG1", snapshot.getKey());
                    }
                }
                Random_SD_Invitation.getFirst_n_users_UID();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void alluserlocation(){
        // Current user's location
        locations.child(currentUser_UID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Get Lat & Lng values (current user)
                curr_lat = dataSnapshot.child("latitude").getValue().toString();
                curr_lng = dataSnapshot.child("longitude").getValue().toString();

                curr_lat_int = Double.parseDouble(curr_lat);
                curr_lng_int = Double.parseDouble(curr_lng);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        // Distance with others users
        locations.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Get Lat & Lng values (selected user)
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    lat = snapshot.child("latitude").getValue().toString();
                    lng = snapshot.child("longitude").getValue().toString();
                    Log.d("PRINT_lat", lat);
                    Log.d("PRINT_lng", lng);
                    Log.d("UID", snapshot.getKey());

                    lat_int = Double.parseDouble(lat);
                    lng_int = Double.parseDouble(lng);

                    double temp = getDistance(lat_int, lng_int);
                    Log.d("Distance", String.valueOf(temp));

                    //Record distance into ArrayList
                    distance.add(temp);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private static double getDistance(double fd_lat, double fd_lng){
        double lat_temp = curr_lat_int;
        double lng_temp = curr_lng_int;
        double fdlat_temp = fd_lat;
        double fdlng_temp = fd_lng;

        float pk = (float) (180.f/Math.PI);

        double a1 = lat_temp / pk;
        double a2 = lng_temp / pk;
        double b1 = fdlat_temp / pk;
        double b2 = fdlng_temp / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        //Return distance in --->  kilometers <-----
        return (6366000 * tt)/1000;
    }

    private static void getFirst_n_users_UID(){
        //看all_UID arrayList內有多少筆data
        Log.d("TAG1_array_size", String.valueOf(all_UID.size()));

        //Remove current users UID in the arrayList
        all_UID.remove(FirebaseAuth.getInstance().getCurrentUser().getUid());

        //記錄最近距離的n個users
        if (all_UID.size() >= Integer.parseInt(invitation_step1.getSelected_num_of_ppl())){
            //取最近距離的n個users
            UID_selected = all_UID.subList(0, Integer.parseInt(invitation_step1.getSelected_num_of_ppl()));
/*
            for (int k = 0; k < UID_selected.size(); k++) {
                Log.d("TAG1_UID", UID_selected.get(k));
            }
 */
        }else
        //Users 數量少於邀約上限人數
        {
            UID_selected = all_UID;
/*
            for (int k = 0; k < UID_selected.size(); k++) {
                Log.d("TAG1_UID", UID_selected.get(k));
            }
 */
        }

        Intent intent = new Intent(mContent, MapsActivity.class);
        mContent.startActivity(intent);
    }

    public static List<String> getUID_selected() {
        return UID_selected;
    }

    public static void clear_List(){
        UID_selected.clear();
    }
}
