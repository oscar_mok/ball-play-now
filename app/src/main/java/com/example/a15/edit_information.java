package com.example.a15;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

public class edit_information extends AppCompatActivity {
    private static String previous_name;
    private static String previous_phone;
    private Button mBtn_finished_edit_info;
    private EditText mEditText_Name;
    private EditText mEditText_phone_num;
    private TextView mEditText_Birthday;
    private TextView mEditTest_register_email;
    private Spinner mSpinner_basketball_level;
    private Spinner mSpinner_football_level;
    private Spinner mSpinner_vollyball_level;
    private CheckBox mCheckBox_Basketball;
    private CheckBox mCheckBox_Football;
    private CheckBox mCheckBox_Volleyball;
    private ImageButton mImageButton;
    private FirebaseUser currentFirebaseUser;
    private String UID;
    private FirebaseDatabase FIREbaseDatebase;
    private DatabaseReference FIREBASEDatebase;
    private String msLevel_basketball;
    private String msLevel_football;
    private String msLevel_vollyball;
    private String new_password;
    private TextInputLayout acc_password,acc_comfirm;
    private ImageView ProfileImage;
    final static int Gallery_Pick = 1;
    private StorageReference UserProfileImageRef;


    public void setmEditText_Name(EditText mEditText_Name) {
        this.mEditText_Name = mEditText_Name;
    }

    public void setmEditText_phone_num(EditText mEditText_phone_num) {
        this.mEditText_phone_num = mEditText_phone_num;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_information);

        ProfileImage = (ImageView)findViewById(R.id.profile_image);
        mImageButton = (ImageButton) findViewById(R.id.imageButton99);
        mEditText_Name = (EditText) findViewById(R.id.editText_Name1);
        mEditText_phone_num = (EditText) findViewById(R.id.editText_phoneNum1);
        acc_password = (TextInputLayout) findViewById(R.id.acc_password);
        acc_comfirm = (TextInputLayout)findViewById(R.id.acc_confirm);
        mSpinner_basketball_level = (Spinner) findViewById(R.id.spinner_basketball_level1);
        mSpinner_football_level = (Spinner) findViewById(R.id.spinner_football_level1);
        mSpinner_vollyball_level = (Spinner) findViewById(R.id.spinner_vollyball_level1);
        mCheckBox_Basketball = (CheckBox) findViewById(R.id.checkBox_Basketball1);
        mCheckBox_Football = (CheckBox) findViewById(R.id.checkBox_Football1);
        mCheckBox_Volleyball = (CheckBox) findViewById(R.id.checkBox_Volleyball1);
        mBtn_finished_edit_info = (Button) findViewById(R.id.btn_finished_edit_info1);

        //get user's UID, Use "currentFirebaseUser.getUid()" to get user's UID
        //or use "currentFirebaseUser.getUid()" to find the correct path of database
        currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;


        //if user is not sign in, pop up warning toast message
        if (currentFirebaseUser == null){

            Toast.makeText(getApplicationContext(), "您尚未登入", Toast.LENGTH_SHORT).show();

        }else {
            //get user's UID, Use "currentFirebaseUser.getUid()" to get user's UID
            //or use "currentFirebaseUser.getUid()" to find the correct path of database
            UID = currentFirebaseUser.getUid();

//            Toast.makeText(this, "" + currentFirebaseUser.getUid(), Toast.LENGTH_SHORT).show();
            Log.d("UID: ", currentFirebaseUser.getUid());

            //Database Reference
            FIREBASEDatebase = FirebaseDatabase.getInstance().getReference();

            //Read current profile image
            FIREBASEDatebase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //ProfileImage
                    if (dataSnapshot.child("User").child(UID).child("ProfileImage").exists()) {
                        String string = dataSnapshot.child("User").child(UID).child("ProfileImage").getValue(String.class);
                        Picasso.get().load(string).into(ProfileImage);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        //Put previous text back to activity (if any)
        mEditText_Name.setText(previous_name);
        mEditText_phone_num.setText(previous_phone);

        mCheckBox_Basketball.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCheckBox_Basketball.isChecked()) {
                    //建立Spinner_basketball_level OnItemSelectedListener
                    mSpinner_basketball_level.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        //if Spinner_basketball_level 有選取了任何值，跑下面程式碼
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            msLevel_basketball = parent.getSelectedItem().toString();

                            Toast.makeText(getApplicationContext(), "您選擇了等級: " + msLevel_basketball, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        //else 沒有選取了任何值
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } //Overwrite OnItemSelectedListener so that OnItemSelectedListener will not work when checkbox is already unchecked
                else {
                    //Reset position of Spinner to starting position
                    mSpinner_basketball_level.setSelection(0);
                    msLevel_basketball = null;
                    mSpinner_basketball_level.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        //if Spinner_basketball_level 有選取了任何值，跑下面程式碼
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            msLevel_basketball = null;

                            Toast.makeText(getApplicationContext(), "您選擇了等級: " + msLevel_basketball, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        });

        mCheckBox_Football.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCheckBox_Football.isChecked()) {
                    //建立Spinner_football_level OnItemSelectedListener
                    mSpinner_football_level.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        //if Spinner_football_level 有選取了任何值，跑下面程式碼
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            msLevel_football = parent.getSelectedItem().toString();

                            Toast.makeText(getApplicationContext(), "您選擇了等級: " + msLevel_football, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        //else 沒有選取了任何值
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    mSpinner_football_level.setSelection(0);
                    msLevel_football = null;
                    mSpinner_football_level.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        //if Spinner_football_level 有選取了任何值，跑下面程式碼
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            msLevel_football = null;

                            Toast.makeText(getApplicationContext(), "您選擇了等級: " + msLevel_football, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        //else 沒有選取了任何值
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        });

        mCheckBox_Volleyball.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCheckBox_Volleyball.isChecked()) {
                    //建立Spinner_vollyball_level OnItemSelectedListener
                    mSpinner_vollyball_level.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        //if Spinner_vollyball_level 有選取了任何值，跑下面程式碼
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            msLevel_vollyball = parent.getSelectedItem().toString();

                            Toast.makeText(getApplicationContext(), "您選擇了等級: " + msLevel_vollyball, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        //else 沒有選取了任何值
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    mSpinner_vollyball_level.setSelection(0);
                    msLevel_vollyball = null;
                    //建立Spinner_vollyball_level OnItemSelectedListener
                    mSpinner_vollyball_level.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        //if Spinner_vollyball_level 有選取了任何值，跑下面程式碼
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            msLevel_vollyball = null;

                            Toast.makeText(getApplicationContext(), "您選擇了等級: " + msLevel_vollyball, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        //else 沒有選取了任何值
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        });

        //photos
        FIREBASEDatebase = FirebaseDatabase.getInstance().getReference().child("User").child(UID);
        UserProfileImageRef = FirebaseStorage.getInstance().getReference().child("Profile Image");

        //點按圖片可編輯頭貼
        ProfileImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gallyeryIntent = new Intent();
                gallyeryIntent.setAction(Intent.ACTION_GET_CONTENT);
                gallyeryIntent.setType("image/*");
                startActivityForResult(gallyeryIntent, Gallery_Pick);
            }
        });



        //當用家按下imagebutton後，顯示等級說明
        mImageButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"運動等級 1～5   1級:最弱  5級:最強",Toast.LENGTH_LONG).show();
            }
        });

        mBtn_finished_edit_info.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                // 2 scenario. 用戶沒有更新他的密碼  OR  用戶有更新他的密碼
                //用戶沒有更新他的密碼
                if (acc_password.getEditText().getText().toString().matches("") && acc_comfirm.getEditText().getText()
                        .toString().matches("")) {


                    //把修改後的個人資料寫入Database

                    //Setting path: User -> UID
                    //currentFirebaseUser.getUid() <--- Can get UID of user's email
                    FIREbaseDatebase = FirebaseDatabase.getInstance();
                    FIREBASEDatebase = FIREbaseDatebase.getReference("User").child(currentFirebaseUser.getUid());

                    if (mEditText_Name.getText().toString().matches("")) {
                    } else {

                        FIREBASEDatebase.child("Name").setValue(mEditText_Name.getText().toString());

                    }

                    if (mEditText_phone_num.getText().toString().matches("")) {
                    } else {

                        FIREBASEDatebase.child("Phone").setValue(mEditText_phone_num.getText().toString());

                    }

                    if (msLevel_basketball == null) {
                    } else {

                        FIREBASEDatebase.child("Basketball_LV").setValue(msLevel_basketball);

                    }

                    if (msLevel_football == null) {
                    } else {

                        FIREBASEDatebase.child("Football_LV").setValue(msLevel_football);

                    }

                    if (msLevel_vollyball == null) {
                    } else {

                        FIREBASEDatebase.child("Volleyball_LV").setValue(msLevel_vollyball);

                    }

                    //Display "會員資料已更新" message
                    Toast.makeText(getApplicationContext(), "會員資料已更新", Toast.LENGTH_SHORT).show();

                    //完成更改，跳回主畫面
                    finish();
                    MainActivity.getInstance().finish();
                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent);


                }else if(acc_password.getEditText().getText().toString().matches("") || acc_comfirm.getEditText().getText()
                        .toString().matches("")){

                    Toast.makeText(getApplicationContext(), "請填寫 確定新密碼 欄", Toast.LENGTH_SHORT).show();

                }
                //用戶有更新他的密碼
                else {

                    //把修改後的個人資料寫入Database

                    //Setting path: User -> UID
                    //currentFirebaseUser.getUid() <--- Can get UID of user's email
                    FIREbaseDatebase = FirebaseDatabase.getInstance();
                    FIREBASEDatebase = FIREbaseDatebase.getReference("User").child(currentFirebaseUser.getUid());
                    new_password = acc_password.getEditText().getText().toString();

                    if (!(new_password.equals(acc_comfirm.getEditText().getText().toString()))) {

                        Toast.makeText(getApplicationContext(), "密碼確認不符", Toast.LENGTH_LONG).show();

                    } else {

                        currentFirebaseUser.updatePassword(new_password).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    //Display "會員資料已更新" message
                                    Toast.makeText(getApplicationContext(), "會員資料已更新", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Error occur. Please try again.", Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                        if (mEditText_Name.getText().toString().matches("")) {
                        } else {

                            FIREBASEDatebase.child("Name").setValue(mEditText_Name.getText().toString());

                        }

                        if (mEditText_phone_num.getText().toString().matches("")) {
                        } else {

                            FIREBASEDatebase.child("Phone").setValue(mEditText_phone_num.getText().toString());

                        }

                        if (msLevel_basketball == null) {
                        } else {

                            FIREBASEDatebase.child("Basketball_LV").setValue(msLevel_basketball);

                        }

                        if (msLevel_football == null) {
                        } else {

                            FIREBASEDatebase.child("Football_LV").setValue(msLevel_football);

                        }

                        if (msLevel_vollyball == null) {
                        } else {

                            FIREBASEDatebase.child("Volleyball_LV").setValue(msLevel_vollyball);

                        }

                        //完成更改，跳回主畫面
                        finish();
                        MainActivity.getInstance().finish();
                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);

                    }
                }
            }
        });
    }

    //crop圖片並存到雲端Storage
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Gallery_Pick && resultCode ==RESULT_OK && data!=null)
        {
            Uri ImageUri = data.getData();
            // start cropping activity for pre-acquired image saved on the device
            CropImage.activity(ImageUri)
                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK)
            {
                Uri resultUri = result.getUri();
                StorageReference filePath = UserProfileImageRef.child(UID+ ".jpg");
                //filePath.putFile(resultUri).addOnCompleteListener(new onCompleteListerner<UploadTask.TaskSnapshot>());
                filePath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(edit_information.this,"Profile image stored successfully to firebase storage..",Toast.LENGTH_SHORT);

                            FirebaseStorage storage = FirebaseStorage.getInstance();
                            String imagename = (UID + ".jpg");
                            String url = "gs://project-ncu-15.appspot.com/Profile Image/" + imagename;
                            StorageReference storageRef = storage.getReferenceFromUrl(url);

                            storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String url = uri.toString();
                                    Log.d("URL", url);
                                    FIREBASEDatebase.child("ProfileImage").setValue(url);
                                    Toast.makeText(edit_information.this,"Profile Image stored to Firebase Database Successfully！",Toast.LENGTH_SHORT).show();

                                    // Prompt after editing Profile Image
                                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which){
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    //Yes button clicked
                                                    MainActivity.getInstance().finish();
                                                    finish();
                                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                    startActivity(intent);
                                                    break;

                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    //No button clicked
                                                    String temp_name = mEditText_Name.getText().toString();
                                                    String temp_phone = mEditText_phone_num.getText().toString();
                                                    finish();
                                                    intent = new Intent(getApplicationContext(), edit_information.class);
                                                    startActivity(intent);
                                                    edit_information.setName_Phone(temp_name, temp_phone);
                                                    break;
                                            }
                                        }
                                    };

                                    AlertDialog.Builder builder = new AlertDialog.Builder(edit_information.this);
                                    builder.setMessage("Profile image have been changed, do you want to finish editing?")
                                            .setPositiveButton("Yes", dialogClickListener)
                                            .setNegativeButton("No", dialogClickListener).show();
                                }
                            });
                        }
                    }
                });
            }
//            else{Toast.makeText(edit_information.this,"Error Occurred: Image can't be cropped. Try Again.",Toast.LENGTH_SHORT).show();}
        }
    }
    public static void setName_Phone(String name, String phone){
        previous_name = name;
        previous_phone = phone;
    }
}
