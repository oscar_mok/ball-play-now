package com.example.a15;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ListOnline2 extends AppCompatActivity {
    private RecyclerView listOnline;
    private LinearLayoutManager layoutManager;
    private DatabaseReference locations;
    private DatabaseReference onlineRef;
    private DatabaseReference counterRef;
    private DatabaseReference currentUserRef;
    private FirebaseRecyclerAdapter<User, ListOnlineViewHolder> adapter;
    private String lat;
    private String lng;
    private static double lat_int, lng_int;
    private String curr_lat;
    private String curr_lng;
    private static double curr_lat_int, curr_lng_int;
//    private List<Double> lat_arrayList = new ArrayList<Double>();
//    private List<Double> lng_arrayList = new ArrayList<Double>();
    private List<Double> distance = new ArrayList<Double>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_online);

        //init view
        listOnline = (RecyclerView)findViewById(R.id.listOnline);
        listOnline.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        listOnline.setLayoutManager(layoutManager);

        //firebase
        locations = FirebaseDatabase.getInstance().getReference("Locations");
        onlineRef = FirebaseDatabase.getInstance().getReference().child(".info/connected");
        counterRef = FirebaseDatabase.getInstance().getReference("lastOnline"); //Create new child name lastOnline
        currentUserRef = FirebaseDatabase.getInstance().getReference("lastOnline").child(FirebaseAuth.getInstance().getCurrentUser().getUid()); //Create new child in lastOnline with key is UID

//        FirebaseDatabase.getInstance().getReference("lastOnline").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("email").setValue(FirebaseAuth.getInstance().getCurrentUser().getEmail());

        //getDistance
        alluserlocation();

        updateList();
    }

    private void updateList() {
        FirebaseRecyclerOptions<User> useroptions = new FirebaseRecyclerOptions.Builder<User>().setQuery(counterRef, User.class).build();
        adapter = new FirebaseRecyclerAdapter<User, ListOnlineViewHolder>(useroptions) {
            @Override
            protected void onBindViewHolder(@NonNull ListOnlineViewHolder listOnlineViewHolder, int i, @NonNull final User user) {
                final String userIDs = getRef(i).getKey();
                listOnlineViewHolder.txtEmail.setText(user.getEmail());
                listOnlineViewHolder.txtDistance.setText("Distance: " + String.valueOf(distance.get(i)) + " (KM)");

                listOnlineViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //If model is current user, not set click event
                        if (!user.getEmail().equals(FirebaseAuth.getInstance().getCurrentUser().getEmail())) {

                            //Current user location 已在 alluserlocation() 取得，不需要重複讀取
                            // Selected user's location 會在MapTracking2.java 取得

                            //Return to current RecyclerView when user pressed "back"
                            finish();
                            Intent intent = new Intent(ListOnline2.this, ListOnline2.class);
                            intent.putExtra("node", userIDs);
                            startActivity(intent);

                            //顯示Map ( MapTracking2.class )
                            Intent map = new Intent(ListOnline2.this, MapTracking2.class);
                            map.putExtra("Email",user.getEmail());
                            map.putExtra("UserID", userIDs);
                            startActivity(map);
                        }else {
                            Toast.makeText(getApplicationContext(), "這是您的帳號",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

            @NonNull
            @Override
            public ListOnlineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                ListOnline2.super.onStart();
                View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.user_layout, parent, false);
                return new ListOnlineViewHolder(view);
            }
        };
        adapter.startListening();
        listOnline.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public class ListOnlineViewHolder extends RecyclerView.ViewHolder {
        public TextView txtEmail, txtDistance;
        public ListOnlineViewHolder(@NonNull View itemView) {
            super(itemView);
            txtEmail = (TextView)itemView.findViewById(R.id.txt_email);
            txtDistance = (TextView)itemView.findViewById(R.id.txt_distance);
        }
    }

    public void alluserlocation(){
        // Current user's location
        locations.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Get Lat & Lng values (current user)
                curr_lat = dataSnapshot.child("latitude").getValue().toString();
                curr_lng = dataSnapshot.child("longitude").getValue().toString();

                Log.d("TAG", curr_lat);
                Log.d("TAG", curr_lng);

                curr_lat_int = Double.parseDouble(curr_lat);
                curr_lng_int = Double.parseDouble(curr_lng);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        // Selected user's location
        locations.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Get Lat & Lng values (selected user)
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    lat = snapshot.child("latitude").getValue().toString();
                    lng = snapshot.child("longitude").getValue().toString();
                    Log.d("PRINT_lat", lat);
                    Log.d("PRINT_lng", lng);

                    lat_int = Double.parseDouble(lat);
                    lng_int = Double.parseDouble(lng);

                    double temp = getDistance(lat_int, lng_int);
                    Log.d("SIZE", String.valueOf(temp));

                    //Record distance into ArrayList
                    distance.add(temp);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public static double getDistance(double fd_lat, double fd_lng){
        double lat_temp = ListOnline2.getCurr_lat_int();
        double lng_temp = ListOnline2.getCurr_lng_int();
        double fdlat_temp = fd_lat;
        double fdlng_temp = fd_lng;

        float pk = (float) (180.f/Math.PI);

        double a1 = lat_temp / pk;
        double a2 = lng_temp / pk;
        double b1 = fdlat_temp / pk;
        double b2 = fdlng_temp / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        //Return distance in --->  kilometers <-----
        return (6366000 * tt)/1000;
    }

    public static double getCurr_lat_int() {
        return curr_lat_int;
    }

    public static double getCurr_lng_int() {
        return curr_lng_int;
    }

    @Override
    protected void onStop(){
        if (adapter != null)
            adapter.stopListening();
        super.onStop();
    }
}