package com.example.a15;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class manage_invitation extends AppCompatActivity implements OnMapReadyCallback, LocationListener {
    private Button mBtn_join_invitation;
    private String invitation_pressed;
    private DatabaseReference invitationRef;
    private TextView mTime, mInitiator, mSportLV, mNum_of_ppl, mAge_restriction, mSex_restriction, mSport;
    private LocationManager locMgr;
    private String bestProv;
    private GoogleMap mMap;
    private Double Longitude = null;
    private Double Latitude = null;
    private String num_ppl_String;
    private int num_ppl_int;
    private String UID;
    private String initiator;
    private List<String> members_uid;
    private List<String> pending_member_uid;

    @Override
    protected void onResume() {
        super.onResume();
        locMgr = (LocationManager)getSystemService(Context.LOCATION_SERVICE);  //取得定位服務
        Criteria criteria = new Criteria();                              //利用criteria物件取得最佳定位方式
        bestProv = locMgr.getBestProvider(criteria, true);  //取得最佳定位方式

        //如果GPS或網路訂位開啟，更新位置
        if (locMgr.isProviderEnabled(LocationManager.GPS_PROVIDER)||
                locMgr.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            //確認ACCESS_FINE_LOCATION 權限是否授權
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
                locMgr.requestLocationUpdates(bestProv, 1000, 1, this);
            }
        }
        else{
            Toast.makeText(this, "請開啟定位服務", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        //確認ACCESS_FINE_LOCATION 權限是否授權
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locMgr.removeUpdates(this);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.manage_invitation);

        mTime = (TextView) findViewById(R.id.time_TV);
        mInitiator = (TextView) findViewById(R.id.initiator_TV);
        mSportLV = (TextView) findViewById(R.id.sportLV_TV);
        mNum_of_ppl = (TextView) findViewById(R.id.numOFppl_TV);
        mAge_restriction = (TextView) findViewById(R.id.age_restriction_TV);
        mSex_restriction = (TextView) findViewById(R.id.sex_TV);
        mSport = (TextView) findViewById(R.id.sport_TV);
        mBtn_join_invitation = (Button) findViewById(R.id.btn_join_invitation);

        UID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        mBtn_join_invitation.setVisibility(View.INVISIBLE);

        invitation_pressed = getIntent().getExtras().get("node").toString();
        //這裡logcat 顯示的是 點擊邀約的節點(node) 的值
        Log.d("TAG", invitation_pressed);
        invitationRef = FirebaseDatabase.getInstance().getReference().child("Invitation");
        //正在連接的路徑  (Firebase database最上面有顯示  可對照)
        Log.d("TAG", invitationRef.getRef().toString());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        String apikey = "AIzaSyA3Fmybtg9af7pwQFLOv8o8xxs3qIFx7Qk";
//                AIzaSyB3l5Ez04aA-GkfNwh1nN0UGb0Ct3gPIX4
        if (!Places.isInitialized()){
            Places.initialize(getApplicationContext(), apikey);
        }

        invitationRef.child(invitation_pressed).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    //正在連接的路徑
                    Log.d("TAG", dataSnapshot.getRef().toString());

                    num_ppl_String = dataSnapshot.child("Number_of_people").getValue().toString();

                    mTime.setText(dataSnapshot.child("Start_Time").getValue().toString() + "~" +
                            dataSnapshot.child("End_Time").getValue().toString());
                    mSportLV.setText(dataSnapshot.child("Sport_Level_Minimum").getValue().toString() + "~" +
                            dataSnapshot.child("Sport_Level_Maximum").getValue().toString());
                    mAge_restriction.setText(dataSnapshot.child("Age_Restriction").getValue().toString());
                    mSex_restriction.setText(dataSnapshot.child("Sex_Restriction").getValue().toString());
                    mSport.setText(dataSnapshot.child("Sport_horizontal").getValue().toString());
                    Longitude = Double.parseDouble(dataSnapshot.child("Place_Longitude").getValue().toString());
                    Latitude = Double.parseDouble(dataSnapshot.child("Place_Latitude").getValue().toString());
                    Log.d("TAG", String.valueOf(Latitude));
                    Log.d("TAG", String.valueOf(Longitude));
                    setMap_location();

                    num_ppl_int = Integer.parseInt(num_ppl_String);

                    GenericTypeIndicator<List<String>> gti = new GenericTypeIndicator<List<String>>() {};

                    //Check if "Members" exist, otherwise will cause force close
                    if (dataSnapshot.child("Members").exists()) {
                        members_uid = dataSnapshot.child("Members").getValue(gti);

                        mNum_of_ppl.setText(members_uid.size() + "/" + num_ppl_String);
                        //Log.d("Selected UID:   ", members_uid.toString());

                        for (int i = 0; i < members_uid.size(); i++) {
                            Log.d("Selected UID:   ", members_uid.get(i));
                        }
                    }//if Members_Pending exist
                    else if (dataSnapshot.child("Members_Pending").exists()) {

                        pending_member_uid = dataSnapshot.child("Members_Pending").getValue(gti);
                        mNum_of_ppl.setText("0" + "/" + num_ppl_String);

                        for (int i = 0; i < pending_member_uid.size(); i++) {
                            Log.d("Pending members UID:   ", pending_member_uid.get(i));
                        }

                    }else {
                        pending_member_uid = null;
                        mNum_of_ppl.setText("0" + "/" + num_ppl_String);

                    }

                    initiator = dataSnapshot.child("Initiator").getValue().toString();


                    FirebaseDatabase.getInstance().getReference().child("User").child(initiator)
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    mInitiator.setText(dataSnapshot.child("Name").getValue().toString());
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        join_invitation_button_activity();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //mBtn_join_invitation.setVisibility(View.INVISIBLE);
        //mBtn_join_invitation.setEnabled(false);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        //取得GoogleMaps物件
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL); //一般地圖

        //LatLng Point = new LatLng(Latitude, Longitude);
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Point,17));


        //檢查授權
        requestPermission();

        mMap.getUiSettings().setZoomControlsEnabled(true); //顯示縮放圖示

        mMap.getUiSettings().setRotateGesturesEnabled(true); //可用手勢控制地圖旋轉
        mMap.getUiSettings().setScrollGesturesEnabled(true); //可用手勢控制地圖左右移動
        mMap.getUiSettings().setZoomGesturesEnabled(true); //可使用手勢控制地圖縮放
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >=23){  //Androids 6.0以上
            // 判斷是否已取得授權
            int hasPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

            if (hasPermission != PackageManager.PERMISSION_GRANTED){ //未取得授權
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
                return;
            }
        }

        //如果裝置版本是Androids 6.0以下
        //如果裝置版本是6.0(包含)以上，使用者已授權

        setMyLocation(); //顯示定位圖層
    }

    private void setMyLocation()throws SecurityException {
        mMap.setMyLocationEnabled(true); //顯示定位圖層
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        //確認授權碼為1才處理
        if (requestCode == 1){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){ //按允許鈕
                setMyLocation(); //顯示定位圖層
            }
            else{ //按拒絕鈕
                Toast.makeText(this,"未取得授權", Toast.LENGTH_SHORT).show();
                finish(); //結束應用程式
            }
        }
        else{
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Criteria criteria = new Criteria();
        bestProv = locMgr.getBestProvider(criteria, true);
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    public void setMap_location(){
        LatLng Point = new LatLng(Latitude, Longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Point,14));
        Marker marker = mMap.addMarker(new MarkerOptions().position(Point).title("邀約位置"));
        marker.setDraggable(false); //標記不可移動
        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)); //更改標記顏色

    }

    protected void join_invitation_button_activity() {
        // 只有3種情況， 1) 有Pending Member + Member == null  2) 有Pending Member + Member != null
        // 3)沒有Pending Member + Member != null
        //   !!!!!!!!!!!!!!!!  沒有Pending Member + Member == null  不可能發生，因為不能在沒有選朋友下發邀約
        // 檢查User是否發起人
        if (UID.matches(initiator)) {
            mBtn_join_invitation.setVisibility(View.INVISIBLE);

        }
        else
        {
            //避免null error
            if (members_uid != null) {
                memberUID_is_null_function();
            }


            if (pending_member_uid != null) {
                Pending_memberUID_is_null_function();
            }
        }
    }

    protected void Pending_memberUID_is_null_function() {
        // 如果有在Pending list, Button 文字會顯示 "接受邀約"
        if (pending_member_uid.contains(UID)) {
            mBtn_join_invitation.setText("接受邀約");
            mBtn_join_invitation.setVisibility(View.VISIBLE);
            mBtn_join_invitation.setEnabled(true);

            mBtn_join_invitation.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //已答應的人數非零
                    if (members_uid != null) {
                        //從Pending list除名
                        pending_member_uid.remove(UID);
                        invitationRef.child(invitation_pressed).child("Members_Pending").setValue(pending_member_uid);

                        //加入到Member list
                        members_uid.add(UID);

                        invitationRef.child(invitation_pressed).child("Members").removeValue();
                        invitationRef.child(invitation_pressed).child("Members").setValue(members_uid);
                        finish();

                        Toast.makeText(getApplicationContext(), "已加入邀約", Toast.LENGTH_LONG).show();
                    } else {
                        //從Pending list除名
                        pending_member_uid.remove(UID);
                        invitationRef.child(invitation_pressed).child("Members_Pending").setValue(pending_member_uid);

                        //加入到Member list
                        invitationRef.child(invitation_pressed).child("Members").child("0").setValue(UID);
                        finish();

                        Toast.makeText(getApplicationContext(), "已加入邀約", Toast.LENGTH_LONG).show();
                    }
                }
            });
        } else {
            //顯示join button
            mBtn_join_invitation.setVisibility(View.VISIBLE);
            mBtn_join_invitation.setEnabled(true);
            mBtn_join_invitation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //已答應的人數非零
                    if (members_uid != null) {
                        //加入到Member list
                        members_uid.add(UID);

                        invitationRef.child(invitation_pressed).child("Members").removeValue();
                        invitationRef.child(invitation_pressed).child("Members").setValue(members_uid);
                        finish();

                        Toast.makeText(getApplicationContext(), "已加入邀約", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }


    protected void memberUID_is_null_function(){
        //避免重複join 邀約 && 已達上限人數
        if (members_uid.contains(UID) || members_uid.size() == num_ppl_int){
            mBtn_join_invitation.setVisibility(View.INVISIBLE);
        }else {
            //顯示join button
            mBtn_join_invitation.setVisibility(View.VISIBLE);
            mBtn_join_invitation.setEnabled(true);
            mBtn_join_invitation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //已答應的人數非零
                    if (members_uid != null) {
                        //加入到Member list
                        members_uid.add(UID);

                        invitationRef.child(invitation_pressed).child("Members").removeValue();
                        invitationRef.child(invitation_pressed).child("Members").setValue(members_uid);
                        finish();

                        Toast.makeText(getApplicationContext(), "已加入邀約", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
}
