package com.example.a15;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.FirebaseRecyclerOptions.Builder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class invitation_step2 extends AppCompatActivity{

    private FirebaseAuth mAuth;
    private String online_user_id;
    private DatabaseReference FriendsRef;
    private DatabaseReference UsersRef;
    private RecyclerView recyclerview;
    private FirebaseRecyclerAdapter<FindFriends, invitation_step2.FriendsViewHolder> adapter;
    private String frienduid;
    private FloatingActionButton mRefresh_FAB;
    private FloatingActionButton mSubmit_FAB;
    private static List<String> UID_selected = new ArrayList<String>();
    private int count = 0;
    private int num_of_ppl;

    // Get selected friends' UID from others java class
    public static List<String> getUID_selected() {
        return UID_selected;
    }

    public static void clear_List(){
        UID_selected.clear();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Clear list values (if any)
        invitation_step2.clear_List();

        setContentView(R.layout.invitation_step2);
        mRefresh_FAB = (com.google.android.material.floatingactionbutton.FloatingActionButton) findViewById(R.id.Refersh_floatingActionButton);
        mSubmit_FAB = (com.google.android.material.floatingactionbutton.FloatingActionButton) findViewById(R.id.Submit_floatingActionButton);

        mAuth = FirebaseAuth.getInstance();
        online_user_id = mAuth.getCurrentUser().getUid();
        FriendsRef = FirebaseDatabase.getInstance().getReference().child("Friends").child(online_user_id);
        UsersRef = FirebaseDatabase.getInstance().getReference().child("User");

        recyclerview = (RecyclerView)findViewById(R.id.invitation_step2_recyclerview);
        //myFriendList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recyclerview.setLayoutManager(linearLayoutManager);

        DisplayAllFriends();

        num_of_ppl = Integer.parseInt(invitation_step1.getSelected_num_of_ppl());

        mRefresh_FAB.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(getApplicationContext(), invitation_step2.class);
                startActivity(intent);
            }
        });

        mSubmit_FAB.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count <= num_of_ppl && count >= 1) {
/*
                    //Display UID selected in Logcat
                    do {
                        Log.d("Selected UID:   ", UID_selected.get(count));
                        count--;
                    } while (count >= 0);

 */

                    Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "請選擇" + num_of_ppl + "位朋友或以下", Toast.LENGTH_SHORT).show();
                    finish();
                    Intent intent = new Intent(getApplicationContext(), invitation_step2.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void DisplayAllFriends() {

        FirebaseRecyclerOptions<FindFriends> options = new Builder<FindFriends>().setQuery(FriendsRef, FindFriends.class).build();

        adapter
                = new FirebaseRecyclerAdapter<FindFriends, invitation_step2.FriendsViewHolder>(options) {


            @NonNull
            @Override
            public invitation_step2.FriendsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                invitation_step2.super.onStart();

                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_inbox, parent, false);

                return new invitation_step2.FriendsViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull final invitation_step2.FriendsViewHolder friendsViewHolder, int i, @NonNull FindFriends findFriends) {
                //Log.d("Output: ", FriendsRef.getKey());
                final String userIDs = getRef(i).getKey();


                UsersRef.child(userIDs).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            final String userName = dataSnapshot.child("Name").getValue().toString();
                            final String e_mail = dataSnapshot.child("Email").getValue().toString();

                            friendsViewHolder.name.setText(userName);
                            friendsViewHolder.email.setText("Email: " + e_mail);


                            //Prevent error if user have no profile image
                            if (dataSnapshot.child("ProfileImage").exists()) {
                                final String profileImage = dataSnapshot.child("ProfileImage").getValue().toString();
                                friendsViewHolder.setProfileImage(getApplicationContext(), profileImage);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                //!!!!  get position of the recyclerview
                //final int position = friendsViewHolder.getAdapterPosition();
                friendsViewHolder.lyt_parent.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        friendsViewHolder.lyt_parent.setBackgroundColor(Color.parseColor("#85b8CB"));
                        //Toast.makeText(getApplicationContext(), "The position is: " + position, Toast.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), "UID:   " + userIDs, Toast.LENGTH_SHORT).show();

                        //避免UID重複加到
                        if (!UID_selected.contains(userIDs)) {
                            UID_selected.add(userIDs);
                        }
                        count++;
                    }
                });
            }
        };
        adapter.startListening();
        recyclerview.setAdapter(adapter);

    }

    private class FriendsViewHolder extends RecyclerView.ViewHolder{
        TextView name, email;
        RelativeLayout lyt_checked, lyt_image;
        View lyt_parent;

        public void setProfileImage(Context ctx, String profileImage){
            ImageView mimage = (ImageView)itemView.findViewById(R.id.image);
            Picasso.get().load(profileImage).into(mimage);
        }

        public FriendsViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.from);
            email = itemView.findViewById(R.id.email);
            lyt_checked = (RelativeLayout) itemView.findViewById(R.id.lyt_checked);
            lyt_image = (RelativeLayout) itemView.findViewById(R.id.lyt_image);
            lyt_parent = (View) itemView.findViewById(R.id.lyt_parent);
        }
/*
        public void setLyt_parent_color_transparent() {
            lyt_parent.setBackgroundColor(Color.TRANSPARENT);
        }

 */

    }

    @Override
    protected void onStop() {
        if(adapter != null)
            adapter.stopListening();
        super.onStop();
    }

}
